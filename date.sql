-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2021 at 12:42 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `date`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add user', 4, 'add_user'),
(14, 'Can change user', 4, 'change_user'),
(15, 'Can delete user', 4, 'delete_user'),
(16, 'Can view user', 4, 'view_user'),
(17, 'Can add content type', 5, 'add_contenttype'),
(18, 'Can change content type', 5, 'change_contenttype'),
(19, 'Can delete content type', 5, 'delete_contenttype'),
(20, 'Can view content type', 5, 'view_contenttype'),
(21, 'Can add session', 6, 'add_session'),
(22, 'Can change session', 6, 'change_session'),
(23, 'Can delete session', 6, 'delete_session'),
(24, 'Can view session', 6, 'view_session'),
(25, 'Can add user profile', 7, 'add_userprofile'),
(26, 'Can change user profile', 7, 'change_userprofile'),
(27, 'Can delete user profile', 7, 'delete_userprofile'),
(28, 'Can view user profile', 7, 'view_userprofile'),
(29, 'Can add messages', 8, 'add_messages'),
(30, 'Can change messages', 8, 'change_messages'),
(31, 'Can delete messages', 8, 'delete_messages'),
(32, 'Can view messages', 8, 'view_messages'),
(33, 'Can add friends', 9, 'add_friends'),
(34, 'Can change friends', 9, 'change_friends'),
(35, 'Can delete friends', 9, 'delete_friends'),
(36, 'Can view friends', 9, 'view_friends');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$260000$HEtviTUIDam6TeXTCPyMZU$f1pjmdd5Pc9gH8O3WZNoM5huTJmGOThcJBA/GaWauC4=', '2021-04-27 10:28:11.833458', 0, 'pratik', '', '', 'pratik@gmail.com', 0, 1, '2021-04-19 13:32:38.793112'),
(2, 'pbkdf2_sha256$216000$koEYM3SNT4Gw$wNbRNJ5qa2ilZtDDLNiW+3KnQsGhMDY+Wb2LeDATCzg=', '2021-04-21 07:11:25.487970', 0, 'raj sharma', '', '', 'raj@gmail.com', 0, 1, '2021-04-19 13:33:26.622862'),
(3, 'pbkdf2_sha256$216000$vcjN3Iy7t6Tz$mk1TSEvgUxicOU22ICo74SIFwBamvNHgyrk3I2bI+jM=', '2021-04-19 13:45:55.862699', 0, 'savan', '', '', 'savan@gmail.com', 0, 1, '2021-04-19 13:45:55.320622'),
(4, 'pbkdf2_sha256$216000$T95Hl3znDCR0$dxUlcNOlc3fZ/G3njLJm8N0yAZeYzyQlyLNz0hzPlYA=', '2021-04-21 12:09:58.246357', 0, 'ankit', '', '', 'ankit@gmail.com', 0, 1, '2021-04-21 10:46:28.305458'),
(5, 'pbkdf2_sha256$216000$R4uoe4FnFNP5$F92lNnyplYc/R/UjgVsQr2N76yq7IbUs8GCRw/9B52Q=', '2021-04-21 10:56:39.720281', 0, 'domdom', '', '', 'dom@gmail.com', 0, 1, '2021-04-21 10:56:39.042357'),
(6, 'pbkdf2_sha256$216000$L6f8nrQYBV35$GnONrwL3+qbxeDWj2uDOQdTOXb1hQvZwGeWMPHHyEzw=', '2021-04-21 11:54:44.996300', 0, 'abcde', '', '', 'abcd@gmail.com', 0, 1, '2021-04-21 11:54:44.715268'),
(7, 'pbkdf2_sha256$216000$LDaGx6dHfdp2$2oZ3G+Ql6+FOTR6pLFHr/LKR9WcyITChRIVOSEB9rLY=', '2021-04-21 12:09:25.689457', 0, 'pratikt', '', '', 'pratikt@gmail.com', 0, 1, '2021-04-21 12:09:25.395082'),
(8, 'pbkdf2_sha256$216000$qmFLo7tJpXuj$D8lDlgsvVH1w22kNnovHMqPVG3jKY+pdMiUl7dmIEOg=', '2021-04-21 16:08:30.998223', 0, 'vrinda', '', '', 'vrinda@gmail.com', 0, 1, '2021-04-21 16:08:29.845879');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chat_friends`
--

CREATE TABLE `chat_friends` (
  `id` int(11) NOT NULL,
  `friend` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chat_friends`
--

INSERT INTO `chat_friends` (`id`, `friend`, `user_id`) VALUES
(1, 1, 2),
(2, 2, 1),
(3, 1, 3),
(4, 3, 1),
(5, 1, 4),
(6, 4, 1),
(7, 4, 5),
(8, 5, 4),
(9, 6, 1),
(10, 1, 6),
(11, 7, 4),
(12, 4, 7),
(13, 8, 1),
(14, 1, 8);

-- --------------------------------------------------------

--
-- Table structure for table `chat_messages`
--

CREATE TABLE `chat_messages` (
  `id` int(11) NOT NULL,
  `description` longtext NOT NULL,
  `time` time(6) NOT NULL,
  `seen` tinyint(1) NOT NULL,
  `receiver_name_id` int(11) NOT NULL,
  `sender_name_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chat_messages`
--

INSERT INTO `chat_messages` (`id`, `description`, `time`, `seen`, `receiver_name_id`, `sender_name_id`) VALUES
(1, 'hello', '19:10:20.515744', 1, 2, 1),
(2, 'man', '19:10:22.401011', 1, 2, 1),
(3, 'how are you', '19:10:44.117246', 1, 1, 2),
(4, 'heey', '19:16:13.745038', 1, 1, 3),
(5, 'i am fine', '12:41:38.599107', 1, 1, 2),
(6, 'hello', '16:16:45.585740', 1, 1, 4),
(7, 'testing messages', '16:17:21.243490', 1, 4, 1),
(8, 'does it work?', '16:17:31.522402', 1, 1, 4),
(9, 'hello man', '17:25:16.477312', 1, 6, 1),
(10, 'i am fine', '17:25:39.190188', 1, 1, 6),
(11, 'hello vruna', '21:39:03.914607', 1, 8, 1),
(12, 'hoow are you', '21:39:26.042986', 1, 1, 8),
(13, 'let me check it', '21:40:54.327807', 1, 1, 8),
(14, 'look the message', '21:41:07.738993', 1, 1, 8);

-- --------------------------------------------------------

--
-- Table structure for table `chat_userprofile`
--

CREATE TABLE `chat_userprofile` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `email` varchar(254) NOT NULL,
  `username` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chat_userprofile`
--

INSERT INTO `chat_userprofile` (`id`, `name`, `email`, `username`) VALUES
(1, 'pratik', 'pratik@gmail.com', 'pratik'),
(2, 'raj', 'raj@gmail.com', 'raj sharma'),
(3, 'savan', 'savan@gmail.com', 'savan'),
(4, 'ankit', 'ankit@gmail.com', 'ankit'),
(5, 'dom', 'dom@gmail.com', 'domdom'),
(6, 'abcd', 'abcd@gmail.com', 'abcde'),
(7, 'pratik', 'pratikt@gmail.com', 'pratikt'),
(8, 'vrinda', 'vrinda@gmail.com', 'vrinda');

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL CHECK (`action_flag` >= 0),
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(9, 'chat', 'friends'),
(8, 'chat', 'messages'),
(7, 'chat', 'userprofile'),
(5, 'contenttypes', 'contenttype'),
(6, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2021-04-19 13:31:24.964240'),
(2, 'auth', '0001_initial', '2021-04-19 13:31:25.270605'),
(3, 'admin', '0001_initial', '2021-04-19 13:31:26.280163'),
(4, 'admin', '0002_logentry_remove_auto_add', '2021-04-19 13:31:26.544231'),
(5, 'admin', '0003_logentry_add_action_flag_choices', '2021-04-19 13:31:26.577004'),
(6, 'contenttypes', '0002_remove_content_type_name', '2021-04-19 13:31:26.725647'),
(7, 'auth', '0002_alter_permission_name_max_length', '2021-04-19 13:31:26.913458'),
(8, 'auth', '0003_alter_user_email_max_length', '2021-04-19 13:31:26.997977'),
(9, 'auth', '0004_alter_user_username_opts', '2021-04-19 13:31:27.040933'),
(10, 'auth', '0005_alter_user_last_login_null', '2021-04-19 13:31:27.149741'),
(11, 'auth', '0006_require_contenttypes_0002', '2021-04-19 13:31:27.165312'),
(12, 'auth', '0007_alter_validators_add_error_messages', '2021-04-19 13:31:27.203887'),
(13, 'auth', '0008_alter_user_username_max_length', '2021-04-19 13:31:27.257429'),
(14, 'auth', '0009_alter_user_last_name_max_length', '2021-04-19 13:31:27.320806'),
(15, 'auth', '0010_alter_group_name_max_length', '2021-04-19 13:31:27.383336'),
(16, 'auth', '0011_update_proxy_permissions', '2021-04-19 13:31:27.417784'),
(17, 'auth', '0012_alter_user_first_name_max_length', '2021-04-19 13:31:27.486713'),
(18, 'chat', '0001_initial', '2021-04-19 13:31:27.714344'),
(19, 'sessions', '0001_initial', '2021-04-19 13:31:28.141986');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('2dvet51sf0wcml73ih0fnmh5ux141wfl', '.eJxVjDsOwjAQBe_iGlmLf2tT0ucMluNd4wBypDipEHeHSCmgfTPzXiKmba1x67zEicRFnMXpdxtTfnDbAd1Tu80yz21dplHuijxol8NM_Lwe7t9BTb1-66C8MSoHkws4G3TxmpGtZiJAhwZAOauxGE-Ws_eMQMoVj5oQAKx4fwC5jjaj:1lYi8x:pJab3iVaEHOdjH4n1ekwsrWkcSOONu434ZwRGmLLhaQ', '2021-05-04 04:37:47.942735'),
('53y3w5m5fjfn0us8sq5vnmigvcjyss6a', '.eJxVjMEOwiAQRP-FsyEFit169N5vILvsIlVDk9KejP-uJD3ocea9mZcKuG857FXWMLO6KFCn344wPqQ0wHcst0XHpWzrTLop-qBVTwvL83q4fwcZa25rBi8OLHlmpt46ZOtogOTGxIYTdN8MNjKgQRwT-AHYk5i-QzqLV-8PC4A4zA:1lZFOx:jaGkUAzGGoStSlYhtfTlsXop_NFJrOcIp0DTx5gOUN0', '2021-05-05 16:08:31.009705'),
('a1rebxc50ns6270zytlexjz59kgyl18m', '.eJxVjMEOwiAQRP-FsyEWl83i0bvfQBZYpGogKe2p8d9tkx70NMm8N7Mqz8tc_NJl8mNSV2XU6bcLHF9Sd5CeXB9Nx1bnaQx6V_RBu763JO_b4f4dFO5lWzskMBKzFYmWLEaCMzsiy2TEMboBDTIBQA4mZJQLDEghbxkwRaM-X9kwN8Y:1lZ71B:1jhrtWOryNcacPpkiS6IF9UpmudgX4kIRG-BsAMn5UQ', '2021-05-05 07:11:25.492238'),
('utat96lrt0j020egrcn4n67ljq53at5z', '.eJxVjEEOwiAQAP_C2ZCGAgsevfsGsuyuUjU0Ke2p8e9K0oNeZyazq4TbWtLWZEkTq7Ny6vTLMtJTahf8wHqfNc11Xaase6IP2_R1ZnldjvZvULCVvnXCFBGNsyF6GrP1EjNBFop-YJLINxEIzo4Q4CvQG4h2EIMG2IJ6fwAEoThc:1lZAX9:6oOWsVqB3I57FqhNeuUWGS9ducK6-qle88vAVF8vpnU', '2021-05-05 10:56:39.727269');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `chat_friends`
--
ALTER TABLE `chat_friends`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chat_friends_user_id_0524c1a6_fk_chat_userprofile_id` (`user_id`);

--
-- Indexes for table `chat_messages`
--
ALTER TABLE `chat_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chat_messages_receiver_name_id_665c8813_fk_chat_userprofile_id` (`receiver_name_id`),
  ADD KEY `chat_messages_sender_name_id_2885f450_fk_chat_userprofile_id` (`sender_name_id`);

--
-- Indexes for table `chat_userprofile`
--
ALTER TABLE `chat_userprofile`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chat_friends`
--
ALTER TABLE `chat_friends`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `chat_messages`
--
ALTER TABLE `chat_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `chat_userprofile`
--
ALTER TABLE `chat_userprofile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `chat_friends`
--
ALTER TABLE `chat_friends`
  ADD CONSTRAINT `chat_friends_user_id_0524c1a6_fk_chat_userprofile_id` FOREIGN KEY (`user_id`) REFERENCES `chat_userprofile` (`id`);

--
-- Constraints for table `chat_messages`
--
ALTER TABLE `chat_messages`
  ADD CONSTRAINT `chat_messages_receiver_name_id_665c8813_fk_chat_userprofile_id` FOREIGN KEY (`receiver_name_id`) REFERENCES `chat_userprofile` (`id`),
  ADD CONSTRAINT `chat_messages_sender_name_id_2885f450_fk_chat_userprofile_id` FOREIGN KEY (`sender_name_id`) REFERENCES `chat_userprofile` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
